import {
    contractRef,
    getMethodArguments,
    toHex,
    getContractArgument,
    getContractId,
    continueTx,
    dataVerified,
    genericClaim,
    claimKey,
    contractIssuer,
    callLegacy,
    tokenizationBlockContractId,
    dataUnverified,
    addMethodHandler,
    SELF_REGISTER_HANDLER_NAME,
    executeHandler,
    NewTx,
    ContractIssuer,
    Context,
    ContractHandlers,
    authenticated,
    isSmartContractIssuer,
} from "@coinweb/contract-kit";
import { selfRegisterHandler } from "@coinweb/self-register";

export const CREATE_METHOD_NAME = "CREATE";
export const CONGRATS_METHOD_NAME = "CONGRATS";

// TODO: add ContractCall into contract-kit
export function callMeBack(issuer: ContractIssuer, tokenName: string): any {
    return {
        contract_input: {
            authenticated: false,
            cost: "0x000000000000000000000000000000000000000000000000000000000007a120",
            data: [
                CONGRATS_METHOD_NAME,
                {
                    name: tokenName,
                },
            ],
        },
        contract_ref: contractRef(issuer, []),
    };
}

function createToken(context: Context): NewTx[] {
    const tokenName = getMethodArguments(context)[1];
    const tokenContent = getMethodArguments(context)[2];
    const initialTokenSupply = toHex(getMethodArguments(context)[3]);
    for (let i = 1; i < context.tx.ops.length; i++) {
        console.log(JSON.stringify(getContractArgument(context.tx, i)));
    }
    let self = getContractId(context.tx);

    const auth = authenticated(context.tx);
    if (!auth || !isSmartContractIssuer(auth.issuer)) {
        throw "Ecdsa contract verified claim should be provided";
    }
    const issuer = auth.issuer;
    const issuerContractId = issuer.FromSmartContract;
    const authData = auth.content;

    // Our account which we use for signing
    const account = authData.body[1];
    
    const data = {
        msg: {
            cmd: {
                CreateTokenUi: {
                    extra_fields: [
                        {
                            content: tokenContent,
                            name: "content",
                        },
                        {
                            content: tokenName,
                            name: "name",
                        },
                    ],
                    memo: "00000000-0000-0000-0000-000000000000",
                    protocol_fields: {
                        initial_token_supply: initialTokenSupply,
                        issuance_type: {
                            Mintable: {
                                minters: [
                                    {
                                        auth: issuerContractId,
                                        payload: account,
                                    },
                                ],
                            },
                        },
                        token_admin: {
                            auth: issuerContractId,
                            payload: account,
                        },
                    },
                },
            },
            on_failure: null,
            on_success: callMeBack(contractIssuer(self), tokenName),
        },
        to_broadcaster: null,
    };

    return [
        continueTx([
            dataVerified(
                genericClaim(claimKey(null, null), null, toHex(1000000)),
                contractIssuer(self),
            ),
            callLegacy(2, tokenizationBlockContractId()),
            dataUnverified(data),
            // Forward ECDSA auth claim
            dataVerified(authData, issuer),
        ]),
    ];
}

function congratsOnCreateToken(context: Context): NewTx[] {
    const tokenName = getMethodArguments(context)[1].name;
    console.log("Congrats! You have created", tokenName);
    return [];
}

export function cwebMain() {
    const module: ContractHandlers = { handlers: {} };

    addMethodHandler(module, CREATE_METHOD_NAME, createToken);
    addMethodHandler(module, CONGRATS_METHOD_NAME, congratsOnCreateToken);
    addMethodHandler(module, SELF_REGISTER_HANDLER_NAME, selfRegisterHandler);

    executeHandler(module);
}
