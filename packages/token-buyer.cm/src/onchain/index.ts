import {
    HashId,
    toHex,
    contractRef,
    getContextCall,
    getContractArgument,
    getContractId,
    contractIssuer,
    continueTx,
    dataVerified,
    dataUnverified,
    genericClaim,
    claimKey,
    callLegacy,
    tokenizationBlockContractId,
    getMethodArguments,
    addMethodHandler,
    SELF_REGISTER_HANDLER_NAME,
    getContextTx,
    executeHandler,
    NewTx,
    extractDataUnverified,
    ContractHandlers,
    ContractIssuer,
    Context,
    authenticated,
    isSmartContractIssuer,
} from "@coinweb/contract-kit";
import { selfRegisterHandler } from "@coinweb/self-register";

export const POPULATE_METHOD_NAME = "POPULATE";
export const BUY_METHOD_NAME = "BUY";
export const CASHBACK_METHOD_NAME = "CASHBACK";

function authPayload(): HashId {
    return "03f752eaab0405eebe0b9db7cefa20cf527662321ec89be3cfccd86677d29df3e0";
}

// TODO: uncomment it when we will add contract functionality to cweb-tool
// function getBuyToken() {
//   return getParameters(scriptArgs[1]).buyToken;
// }
// function getCashbackToken() {
//   return getParameters(scriptArgs[1]).cashbackToken;
// }

function getBuyToken() {
    // Carbon token
    return "0x7c4c5ceb0474981dff26c8bf56fa203f8088ff517e5b3641dfdc80d50fe5c47f";
}

function getCashbackToken() {
    // STBL token
    return "0xc7fc0611c0079390b95c472d8b9d153c6468339e83a24d410e95e56b86c1317f";
}

export function cashbackCallback(issuer: ContractIssuer, account: HashId, amount: number): any {
    return {
        contract_input: {
            authenticated: false,
            cost: toHex(80000),
            data: [CASHBACK_METHOD_NAME, account, amount],
        },
        contract_ref: contractRef(issuer, []),
    };
}

function populateCashback(context: Context): NewTx[] {
    const contextCall = getContextCall();
    const token = getCashbackToken();
    for (let i = 1; i < context.tx.ops.length; i++) {
        console.log(JSON.stringify(getContractArgument(context.tx, i)));
    }
    let self = contractIssuer(getContractId(context.tx));
    const providedCwebClaim = extractDataUnverified(getContractArgument(context.tx, 2));
    if (!providedCwebClaim) {
        throw "Expected to get provided cweb DataOp for contract execution";
    }
    const providedCweb = parseInt(providedCwebClaim.toString(), 16);
    console.log("Cweb for contract: ", providedCweb);
    const childCweb = 100000;
    const amount = providedCweb - childCweb;
    const limit = 0;
    const data = {
        msg: {
            cmd: {
                BuyViaCwebUi: {
                    cweb_amount: toHex(amount),
                    limit_token_amount: toHex(limit),
                    token: token,
                },
            },
            on_failure: null,
            on_success: null,
        },
        to_broadcaster: null,
    };

    return [
        continueTx([
            dataVerified(genericClaim(claimKey(null, null), null, toHex(providedCweb)), self),
            callLegacy(2, tokenizationBlockContractId()),
            dataUnverified(data),
            // Create Auth claim
            dataVerified(
                genericClaim(
                    claimKey(null, null),
                    [
                        {
                            input_id: toHex(0),
                            l2_txid: contextCall.txid,
                        },
                        authPayload(),
                    ],
                    toHex(0),
                ),
                self,
            ),
        ]),
    ];
}

function buyToken(context: Context): NewTx[] {
    const token = getBuyToken();
    for (let i = 1; i < context.tx.ops.length; i++) {
        console.log(JSON.stringify(getContractArgument(context.tx, i)));
    }
    let self = getContractId(context.tx);

    const auth = authenticated(context.tx);
    if (!auth || !isSmartContractIssuer(auth.issuer)) {
        throw "Ecdsa contract verified claim should be provided";
    }
    const issuer = auth.issuer;
    const authData = auth.content;

    // Our account which we use for signing
    const account = authData.body[1];

    const providedCwebClaim = extractDataUnverified(getContractArgument(context.tx, 2));

    if (!providedCwebClaim) {
        throw "Expected to get provided cweb DataOp for contract execution";
    }
    const providedCweb = parseInt(providedCwebClaim.toString(), 16);
    console.log("Cweb for buying: ", providedCweb);
    const childCweb = 100000;
    const amount = providedCweb - childCweb;
    // TODO: better fetch price of token
    const limit = 0;
    const data = {
        msg: {
            cmd: {
                BuyViaCwebUi: {
                    cweb_amount: toHex(amount),
                    limit_token_amount: toHex(limit),
                    token: token,
                },
            },
            on_failure: null,
            on_success: cashbackCallback(contractIssuer(self), account, amount),
        },
        to_broadcaster: null,
    };

    return [
        continueTx([
            dataVerified(
                genericClaim(claimKey(null, null), null, toHex(providedCweb)),
                contractIssuer(self),
            ),
            callLegacy(2, tokenizationBlockContractId()),
            dataUnverified(data),
            // Forward ECDSA auth claim
            dataVerified(authData, issuer),
        ]),
    ];
}

function cashbackHandler(context: Context): NewTx[] {
    const contextCall = getContextCall();

    const auth = authenticated(context.tx);
    if (!auth || !isSmartContractIssuer(auth.issuer)) {
        throw "Ecdsa contract verified claim should be provided";
    }
    const issuer = auth.issuer;
    const issuerContractId = issuer.FromSmartContract;
    const authData = auth.content;
    const account = authData.body[1];

    // We take 1% for cashback
    const amount = getMethodArguments(context)[2] / 100;
    const token = getCashbackToken();
    const childCweb = 50000;
    let self = contractIssuer(getContractId(context.tx));
    console.log("Send cashback! Token: ", token, ", Amount - ", amount);
    const data = {
        msg: {
            cmd: {
                TransferTokenUi: {
                    token: token,
                    amount: toHex(amount),
                    receiver: {
                        auth: issuerContractId,
                        payload: account,
                    },
                    memo: "00000000-0000-0000-0000-000000000000",
                    auto_limit_sell_token: null,
                },
            },
            on_failure: null,
            on_success: null,
        },
        to_broadcaster: null,
    };

    return [
        continueTx([
            dataVerified(genericClaim(claimKey(null, null), null, toHex(childCweb)), self),
            callLegacy(2, tokenizationBlockContractId()),
            dataUnverified(data),
            // Create Auth claim
            dataVerified(
                genericClaim(
                    claimKey(null, null),
                    [
                        {
                            input_id: toHex(0),
                            l2_txid: contextCall.txid,
                        },
                        authPayload(),
                    ],
                    toHex(0),
                ),
                self,
            ),
        ]),
    ];
}

export function cwebMain() {
    const module: ContractHandlers = { handlers: {} };

    addMethodHandler(module, POPULATE_METHOD_NAME, populateCashback);
    addMethodHandler(module, BUY_METHOD_NAME, buyToken);
    addMethodHandler(module, CASHBACK_METHOD_NAME, cashbackHandler);
    addMethodHandler(module, SELF_REGISTER_HANDLER_NAME, selfRegisterHandler);
    const contextTx = getContextTx();
    executeHandler(module);
}
